package v1.post

import sys.process._
import javax.inject.Inject
import scala.language.postfixOps

import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class PostFormInput(title: String, body: String)

/**
  * Takes HTTP requests and produces JSON.
  */
class PostController @Inject()(cc: PostControllerComponents)(
    implicit ec: ExecutionContext)
    extends PostBaseController(cc) {

  private val logger = Logger(getClass)

  private val form: Form[PostFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "title" -> nonEmptyText,
        "body" -> text
      )(PostFormInput.apply)(PostFormInput.unapply)
    )
  }

  def index: Action[AnyContent] = PostAction.async { implicit request =>
    logger.trace("index: ")
    postResourceHandler.find.map { posts =>
      Ok(Json.toJson(posts))
    }
  }

  def process: Action[AnyContent] = PostAction.async { implicit request =>
    logger.trace("process: ")
    processJsonPost()
  }

  def show(id: String): Action[AnyContent] = PostAction.async {
    implicit request =>
      logger.trace(s"show: id = $id")
      postResourceHandler.lookup(id).map { post =>
        Ok(Json.toJson(post))
      }
  }

    def topn(n: String) = Action {
    // implicit request =>
      logger.trace(s"process: $n")

      // Clear input file directory
      val exitCodeDelete = Process(s"""gsutil rm gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/uploads/**""").!
      logger.trace(s"EXITCODE TO DELETE FILES: $exitCodeDelete")
      
      // Clear Output directory
      val exitCodeDeleteOutputDir = Process(s"""gsutil rm gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/output/hifreq_top20/**""").!
      logger.trace(s"EXITCODE TO DELETE OUTPUT FILES: $exitCodeDeleteOutputDir")
      
      // Upload input files to input dir on GCP
      val exitCodeUpload = Process(s"""gsutil cp -r /Users/singh/Desktop/Fall2019-20/CS1699/finalproject/rest_service/play-scala-rest-api-example/app/v1/uploads gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/""").!
      logger.trace(s"EXITCODE TO UPLOAD FILES: $exitCodeUpload")
      
      // run job
      val exitCode = Process(s"""gcloud dataproc jobs submit spark
    --cluster cluster-hw4 --region us-central1
    --class SimpleApp
    --jars gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/JAR/simple-project_2.11-1.0.jar
    -- topn $n""").!

      // Get results of job
      val output = Process(s"gsutil cat gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/output/hifreq_top20/*").!!

      Ok(s"$output")
  }
  def searchTerm(term: String) = Action {
      val exitCodeDelete = Process(s"""gsutil rm gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/uploads/**""").!
      logger.trace(s"EXITCODE TO DELETE FILES: $exitCodeDelete")
      
      val exitCodeDeleteOutputDir = Process(s"""gsutil rm gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/output/searchTerm/**""").!
      logger.trace(s"EXITCODE TO DELETE OUTPUT FILES: $exitCodeDeleteOutputDir")
      
      val exitCodeUpload = Process(s"""gsutil cp -r /Users/singh/Desktop/Fall2019-20/CS1699/finalproject/rest_service/play-scala-rest-api-example/app/v1/uploads gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/""").!
      logger.trace(s"EXITCODE TO UPLOAD FILES: $exitCodeUpload")
      val exitCode = Process(s"""gcloud dataproc jobs submit spark
    --cluster cluster-hw4 --region us-central1
    --class SimpleApp
    --jars gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/JAR/simple-project_2.11-1.0.jar
    -- searchTerm $term""").!
    val output = Process(s"gsutil cat gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/output/searchTerm/*").!!
    Ok(s"$output")
  }

  private def processJsonPost[A]()(
      implicit request: PostRequest[A]): Future[Result] = {
    def failure(badForm: Form[PostFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: PostFormInput) = {
      postResourceHandler.create(input).map { post =>
        Created(Json.toJson(post)).withHeaders(LOCATION -> post.link)
      }
    }

    form.bindFromRequest().fold(failure, success)
  }
}


