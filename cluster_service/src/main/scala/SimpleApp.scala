import org.apache.spark.sql.SparkSession

object SimpleApp {
  def main(args: Array[String]) {
   if(args(0) == "topn") {
     topN(args(1))
   } else {
     searchTerm(args(1))
   }
  }
  def searchTerm(term: String) {
    // val rpscricket=rpsteam.flatMap(lines=>lines.split(" ")).filter(value=>value=="Rahane").map(word=>(word,1)).reduceByKey(_+_);
   
    val spark = SparkSession.builder.appName("Simple Application").getOrCreate()
    val sc = spark.sparkContext
    val textRDD = sc.wholeTextFiles("gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/uploads/**")

    val lis = textRDD.collect().toList.map(x=>(x._1 + "\t" + x._2))
    val file = sc.parallelize(lis)  


    val firstPass = file.map(x=>(x.split("\t")(0), x.split("\t")(1)))
    val tuples = firstPass.flatMap(x=>x._2.split("\\s+").filter(z=> z == term).map(y=>(x._1, 1))).reduceByKey(_ + _)
    val term_swap = tuples.map(_.swap)
    val termsSorted = term_swap.sortByKey(false,1)

    termsSorted.saveAsTextFile("gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/output/searchTerm")
  }
  def topN(n: String ) = {
    val spark = SparkSession.builder.appName("Simple Application").getOrCreate()
    val sc = spark.sparkContext

    import spark.implicits._
    // val data = spark.read.text("src/main/resources/data.txt").as[String]
    val f = sc.textFile("gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/uploads/*/*")
    // word count 
    val wc = f.flatMap(l => l.split(" ")).map(word => (word,1)).reduceByKey(_ + _)
    
    // swap k,v to v,k to sort by word frequency
    val wc_swap = wc.map(_.swap)
    // sort keys by ascending=false (descending)
    val hifreq_words = wc_swap.sortByKey(false,1)
    // get an array of top 20 frequent words
    val topn = n.toInt
    val top20 = hifreq_words.take(topn)
    top20.foreach(println)
    // convert array to RDD
    val top20rdd = sc.parallelize(top20)
    top20rdd.saveAsTextFile("gs://dataproc-65210b67-6ca6-46ea-a9a5-760c30a8b174-us-central1/output/hifreq_top20")

    spark.stop()
   }
}

// $SPARK_HOME/bin/spark-submit \
//   --class "SimpleApp" \
//   --master local[4] \
//   target/scala-2.12/simple-project_2.12-1.0.jar