from pyspark.sql import SparkSession
from pyspark import SparkContext
from operator import add
# spark = SparkSession\
#         .builder\
#         .appName("PythonWordCount")\
#         .getOrCreate()

sc = SparkContext("local", "First App")

# lines = spark.read.text("/Users/singh/Desktop/Fall2019-20/CS1699/proj_rest/play-scala-rest-api-example/app/v1/uploads").rdd.map(lambda r: (r[0], r[1]))

# lines = sc.wholeTextFiles("/Users/singh/Desktop/Fall2019-20/CS1699/proj_rest/play-scala-rest-api-example/app/v1/uploads")

# print(lines.take(3))

# counts = lines.map(lambda line: (line[0],line[1])) (lambda lines: lines[1].split(" ")).filter(lambda value: value == "Nik").map(lambda word: (word,1)) #.reduceByKey(_+_)

# counts = lines.flatMap(lambda lines: (lines[0], lines[1].split(" ")))
# counts.foreach(lambda i,a: print(i))

# Dummy data load
file = sc.parallelize(list(["doc_1\tnew york city","doc_2\train rain go away"]))  

# Split the data on tabs to get an array of (key, line) tuples
firstPass = file.map(lambda t: t.split("\t"))

secondPass = firstPass.map(x=>(x(0), x(1).split("\\s+").map(y=>(y,1)))) 

//Now group the words and re-map so that the inner tuple is the wordcount
val finalPass = secondPass.map(x=>(x._1, x._2.groupBy(_._1).map(y=>(y._1,y._2.size))))




# .flatMap(lambda l: [(l[0], value) for value in l[1]])
# counts=lines
 #.filter(lambda value: value == "Nik").map(lambda word: (word,1)) #.reduceByKey(_+_)

# counts = lines.flatMap(lambda x: x[1].split(" ")).map(lambda word: x[0] + word ,x[1])
                # .map(lambda x: (x[1], 1)) \
                # .reduceByKey(add)
# print(sc.parallelize(counts).take(4))
# output = counts.collect()
# for (word, count) in output:
    # print("%s: %i" % (word, count))

sc.stop()