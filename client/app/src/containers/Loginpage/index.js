import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

class LoginScreenContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      username: "",
      files: [],
      algorithm: "topn",
      content: "upload",
      results: [],
      headerText: 'Load my engine'
    }
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.handleSubmitFiles = this.handleSubmitFiles.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.renderTabs = this.renderTabs.bind(this);
    this.renderTextInput = this.renderTextInput.bind(this);
    this.renderUploadForm = this.renderUploadForm.bind(this);
    this.handleSubmitAlg = this.handleSubmitAlg.bind(this);
    this.handleTextFieldChange = this.handleTextFieldChange.bind(this);
    this.renderTopn = this.renderTopn.bind(this);
    this.renderSearch = this.renderSearch.bind(this);
  }
  onFormSubmit(e){
            e.preventDefault();
            const formData = new FormData();
            for(var x = 0; x<this.state.files.length; x++) {
                formData.append('file', this.state.files[x])
            }
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            };
            axios.post("/file",formData,config)
                .then((response) => {
                    alert("The file is successfully uploaded");
                    this.setState({content: "algorithm", headerText: "Select Action"})
                }).catch((error) => {
            });
        }
onChangeHandler(event){
    console.log(event.target.files[0])
    this.setState({files: event.target.files})
}

handleSubmitFiles(){
    const data = new FormData()

    for(var x = 0; x<this.state.files.length; x++) {
        data.append('file', this.state.files[x])
    }
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    };
      
    axios.post("/file", data, config, { // receive two parameter endpoint url ,form data 
    })
    .then(res => { // then print response status
        console.log(res.statusText)
    })

    }
    handleChange (event, newValue) {
        console.log(newValue)
        this.setState({"algorithm": newValue, results: []})
      };
  renderTabs() {
    if(this.state.content != 'algorithm') return null
    return (
        <Paper>
            <Tabs 
            value={this.state.algorithm} 
            onChange={this.handleChange}
            aria-label="wrapped label tabs example" 
            indicatorColor="primary"
            textColor="primary">
                <Tab value="topn" label="Top N" />
                <Tab value="Enter Search Term" label="Search Term"  />
            </Tabs>
        </Paper>
    )
  }
  handleSubmitAlg(e) {
      e.preventDefault();
      var endpoint = this.state.algorithm == 'topn' ? '/topn' : '/searchTerm'
      axios.get(endpoint, {params: {
        param: this.state.textFieldValue
      }})
      .then(res => {
        console.log(res.data)
        this.setState({"results": res.data})
      })
  }
  handleTextFieldChange(e) {
    this.setState({
        textFieldValue: e.target.value
    });
}
  renderTextInput() {
    if(this.state.content != 'algorithm') return null
      return (
        <form noValidate autoComplete="off" onSubmit={this.handleSubmitAlg} style={{ display: 'flex', alignItems: 'center', margin: '0 auto', }}>
            <TextField id="outlined-basic" label={this.state.algorithm} variant="outlined" onChange={this.handleTextFieldChange}/>
            <button type="submit" style={{color: 'white', display: 'flex', backgroundColor: '#4da6ff', height: '40px', alignItems: 'center', margin: '0 auto', width: '15%', textAlign: 'center', borderRadius: '5px', marginTop: '20px'}}>
                    <div style={{margin: '0 auto'}}>
                        Submit
                    </div>
                </button>
        </form>
      )
  }
  renderUploadForm(files) {
    if(this.state.content != 'upload') return null
    return (
        <form style={{textAlign: 'center'}} onSubmit={this.onFormSubmit}>
            <div style={{color: 'white', display: 'flex', backgroundColor: '#4da6ff', height: '40px', alignItems: 'center', margin: '0 auto', width: '15%', textAlign: 'center', borderRadius: '5px', marginTop: '20px'}}>
                <div style={{margin: '0 auto'}}>
                    Choose Files
                    <input type="file" name="file" onChange={this.onChangeHandler} multiple/>
                </div>
            </div>
            <div style={{color: 'white', display: 'flex', height: '40px', alignItems: 'center', margin: '0 auto', width: '25%', textAlign: 'center', borderRadius: '5px', marginTop: '20px'}}>
                {files }
            </div>

            {/* <Link to="/home" style={{ textDecoration: 'none' }}> */}
                <button type="submit" style={{color: 'white', display: 'flex', backgroundColor: '#4da6ff', height: '40px', alignItems: 'center', margin: '0 auto', width: '15%', textAlign: 'center', borderRadius: '5px', marginTop: '20px'}}>
                    <div style={{margin: '0 auto'}}>
                        Upload
                    </div>
                </button>
            {/* </Link> */}
        </form>
    )
  }
  renderSearch() {
      return (
        <Paper>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Doc Folder</TableCell>
              <TableCell align="center">Doc Name</TableCell>
              <TableCell align="center">Frequencies</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.results.map(row => (
              <TableRow key={row.name}>
                <TableCell align="center">{row.docFolder}</TableCell>
                <TableCell align="center">{row.value}</TableCell>
                <TableCell align="center">{row.count}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
      )
  }
  renderTopn() {
    return (
      <Paper>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Word</TableCell>
            <TableCell align="right">Count</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.state.results.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.value}
              </TableCell>
              <TableCell align="right">{row.count}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    )
}
  render() {
    var filesList = []
    Array.from(this.state.files).forEach(file => filesList.push(file));
    var files = filesList.map((item, key) =>
        <li key={item.name} style={{ color: '#757575', fontWeight: 'light', fontSize: 15, flex: 0.5, "list-style-type": 'none' }}>{item.name}</li>
    );
    return (
        <div>
            <div style={{ border: '1px solid #E0E0E0', paddingLeft: 20, paddingRight: 20, height: 65, display: 'flex', flexDirection: 'row' }}>
                <p style={{ color: '#757575', fontWeight: 'light', fontSize: 21, flex: 1 }}>Cloud Computing Final</p>
            </div>
            <p style={{ color:'#757575', fontWeight: '200', fontSize: 32, marginTop: 50, textAlign: 'center' }}>{this.state.headerText}</p>
            {this.renderUploadForm(files)}
            {this.renderTabs()}
            {this.renderTextInput()}
            {this.state.algorithm == 'topn' ? this.renderTopn() : this.renderSearch()}
        </div>
    );
  }
}

export default LoginScreenContainer;
