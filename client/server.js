const express = require('express');
const https = require('https');
const path = require("path");
const fs = require('fs');
const bodyParser = require('body-parser');
var multer = require('multer')
var request=require('request');
var targz = require('targz');
const app = express();
const port = process.env.PORT || 5000;
var rimraf = require("rimraf");

const INPUT_WORKING_DIR = '/Users/singh/Desktop/Fall2019-20/CS1699/finalproject/rest_service/play-scala-rest-api-example/app/v1/uploads/'

app.use(bodyParser.json());

const storage = multer.diskStorage({
	// destination: "./public/uploads/",
	destination: INPUT_WORKING_DIR,
	filename: function(req, file, cb){
	   cb(null,file.originalname);
	}
 });
 
var upload = multer({ storage: storage }).array('file')


app.post('/file', (req, res) => {
	rimraf.sync(INPUT_WORKING_DIR + '*');
	upload(req, res, (err) => {
		// console.log(req)
		for(const file of req.files) {
			if(file.originalname.includes("tar.gz")) {
				targz.decompress({
					src: '/Users/singh/Desktop/Fall2019-20/CS1699/finalproject/client/app/data_files/' + file.originalname,
					dest: '/Users/singh/Desktop/Fall2019-20/CS1699/finalproject/rest_service/play-scala-rest-api-example/app/v1/uploads/'
				}, function(err){
					if(err) {
						console.log(err);
						throw err
					} else {
						rimraf.sync(INPUT_WORKING_DIR + '*.tar.gz');
					}
				});
			}
		}
		console.log("Request ---", req.body);
		console.log("Request file ---", req.files);
		if(!err) {
		   return res.send(200).end();
		}
	 });
});

app.get('/topn', (req, res) => {
	console.log("REQ")
	console.log(req.query.param)
	requestURL = 'http://localhost:9000/v1/posts/topn/' + req.query.param
	console.log(requestURL)
	request.get(requestURL,function(err,resp,body){
  		if(err) { 
			  console.log("ERRR")
			  console.log(err)
			  console.log(body)
		  }
		if(res.statusCode !== 200 ) return "error"//etc


		var bodySplit = body.split("\n")
		var values = []
		for(var i = 0; i < bodySplit.length; i++) {
		  var strippedStr = bodySplit[i].substring(1, bodySplit[i].length-1)
		  var commadIndex = strippedStr.indexOf(",")
		  var firstValue = strippedStr.substring(0, commadIndex)
		  var secondValue = strippedStr.substring(commadIndex+1, strippedStr.length)
		  var jsonItem = {"value": secondValue, "count": firstValue}
		  if(jsonItem.count != "")
		  	values.push(jsonItem)
		}

		res.send(values)
	});
});

app.get('/searchTerm', (req, res) => {
	// http://localhost:9000/v1/posts/topn/3
	console.log("REQ")
	console.log(req.query.param)
	requestURL = 'http://localhost:9000/v1/posts/searchTerm/' + req.query.param
	console.log(requestURL)
	request.get(requestURL,function(err,resp,body){
  		if(err) throw err
		if(res.statusCode !== 200 ) return "error"//etc

		console.log(body)
		var bodySplit = body.split("\n")
		var values = []
		for(var i = 0; i < bodySplit.length; i++) {
		  var strippedStr = bodySplit[i].substring(1, bodySplit[i].length-1)
		  var commadIndex = strippedStr.indexOf(",")
		  var firstValue = strippedStr.substring(0, commadIndex)
		  
		  var fullLink = strippedStr.substring(commadIndex+1, strippedStr.length)
		  var splitLink = fullLink.split("/")
		  secondValue = splitLink[splitLink.length - 1]
		  docFolder = splitLink[splitLink.length - 2]
		  var jsonItem = {"value": secondValue, "count": firstValue, "docFolder": docFolder}
		  if(jsonItem.count != "")
		  	values.push(jsonItem)
		}

		res.send(values)
	});
});

app.listen(port, () => console.log(`Listening on port ${port}`));